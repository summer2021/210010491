#include<stdio.h>

#include<libvirt/libvirt.h>

 void getBlockName(virTypedParameterPtr params,int nparams){
     const char * value;
     const char * name="block.0.name";
     int ret;
     ret=virTypedParamsGetString(params,nparams,name,&value);
     if(ret==1){
         printf("name:%s\n",value);
     }
}

void getBlockRdReqs(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.rd.reqs";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("rd.reps:%ld\n",value[0]);
    }
}

void getBlockRdBytes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.rd.bytes";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("rd.bytes:%ld\n",value[0]);
    }
}

void getBlockRdTimes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.rd.times";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("rd.times:%ld\n",value[0]);
    }
}

void getBlockWrReps(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.wr.reqs";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("wr.reps:%ld\n",value[0]);
    }
}

void getBlockWrBytes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.wr.bytes";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("wr.bytes:%ld\n",value[0]);
    }
}

void getBlockWrTimes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.wr.times";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("wr.times:%ld\n",value[0]);
    }
}

void getBlockFlReps(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.fl.reqs";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("fl.reps:%ld\n",value[0]);
    }
}

void getBlockFlTimes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.fl.times";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("fl.times:%ld\n",value[0]);
    }
}


int getDomainInfo(void) {
    int ret,i;

    unsigned int stats=VIR_DOMAIN_STATS_BLOCK;

    unsigned int flags=VIR_CONNECT_GET_ALL_DOMAINS_STATS_ACTIVE|VIR_CONNECT_GET_ALL_DOMAINS_STATS_INACTIVE;

    virConnectPtr conn = NULL;

    virDomainPtr dom = NULL;

    virDomainStatsRecordPtr *retStats;

    virDomainInfo info;
 

    conn = virConnectOpenReadOnly(NULL);
     
    if (conn == NULL) {

        fprintf(stderr, "Failed to connect to hypervisor\n");

        return 1;

    }


   ret = virConnectGetAllDomainStats(conn,stats,&retStats,flags);
printf("%d\n",ret);
   for(i=0;i<ret;i++){
       dom=retStats[i]->dom;
       const char *dom_name;
       dom_name = virDomainGetName(dom);
       printf("%s\n",dom_name);


        unsigned int value;
        const char * name="block.count";
        virTypedParameterPtr params;
        int aaa,nparams;
        params=retStats[i]->params;
	    nparams=retStats[i]->nparams;
        aaa=virTypedParamsGetUInt(params,nparams,name,&value);
	    if(aaa==1){
            printf("count:%d\n",value);
	    }
	    getBlockName(params,nparams);
       getBlockRdReqs(params,nparams);
       getBlockRdBytes(params,nparams);
       getBlockRdTimes(params,nparams);
       getBlockWrReps(params,nparams);
       getBlockWrBytes(params,nparams);
       getBlockWrTimes(params,nparams);
       getBlockFlReps(params,nparams);
       getBlockFlTimes(params,nparams);
   }
    

    virDomainStatsRecordListFree(retStats);


    if (conn != NULL){

        virConnectClose(conn);

    }

    return 0;

}

int main(int argc, char **argv)

{

    printf("-----Get blocky via libvirt C API -----\n");

    getDomainInfo();

    return 0;

}

