#ifndef _BLOCK_H
#define _BLOCK_H

#include<libvirt/libvirt.h>


long int getBlockRdReps(virTypedParameterPtr params,int nparams);
long int getBlockRdBytes(virTypedParameterPtr params,int nparams);
long int getBlockRdTimes(virTypedParameterPtr params,int nparams);
long int getBlockWrReps(virTypedParameterPtr params,int nparams);
long int getBlockWrBytes(virTypedParameterPtr params,int nparams);
long int getBlockWrTimes(virTypedParameterPtr params,int nparams);
long int getBlockFlReps(virTypedParameterPtr params,int nparams);
long int getBlockFlTimes(virTypedParameterPtr params,int nparams);




#endif
