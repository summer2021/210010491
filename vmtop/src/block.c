#include<stdio.h>

#include<libvirt/libvirt.h>

#include<block.h>

long int getBlockRdBytes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.rd.bytes";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getBlockRdReps(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.rd.reps";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getBlockRdTimes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.rd.times";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getBlockWrReps(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.wr.reqs";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getBlockWrBytes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.wr.bytes";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getBlockWrTimes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.wr.times";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getBlockFlReps(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.fl.reqs";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getBlockFlTimes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="block.0.fl.times";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}


