#ifndef _NETWORK_H
#define _NETWORK_H


#include<libvirt/libvirt.h>


extern const virTypedParameterPtr params;
extern const int nparams;

long int getNetworkRxDrop(virTypedParameterPtr params,int nparams);
long int getNetworkRxErrs(virTypedParameterPtr params,int nparams);
long int getNetworkRxPkts(virTypedParameterPtr params,int nparams);
long int getNetworkTxBytes(virTypedParameterPtr params,int nparams);
long int getNetworkTxDrop(virTypedParameterPtr params,int nparams);
long int getNetworkTxErrs(virTypedParameterPtr params,int nparams);
long int getNetworkTxPkts(virTypedParameterPtr params,int nparams);
int getDomainParams(char *name);
long int getNetworkRxBytes(virTypedParameterPtr params,int nparams);



#endif
