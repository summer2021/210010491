#include<stdio.h>

#include<libvirt/libvirt.h>

#include <network.h>

#include <string.h>



long int getNetworkRxBytes(virTypedParameterPtr params,int nparams){
     unsigned long long value[1];
     const char * name="net.0.rx.bytes";
     int ret;
     ret=virTypedParamsGetULLong(params,nparams,name,value);
     if(ret==1){
         return value[0];
     }
}

long int getNetworkRxPkts(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.rx.pkts";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getNetworkRxErrs(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.rx.errs";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getNetworkRxDrop(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.rx.drop";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getNetworkTxBytes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.tx.bytes";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getNetworkTxPkts(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.tx.pkts";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getNetworkTxErrs(virTypedParameterPtr params,int nparams){

    unsigned long long value[1];
    const char * name="net.0.tx.errs";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}

long int getNetworkTxDrop(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.tx.drop";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        return value[0];
    }
}k





int getDomainParams(char *name) {
    int ret,i;

    unsigned int stats=VIR_DOMAIN_STATS_INTERFACE;

    unsigned int flags=VIR_CONNECT_GET_ALL_DOMAINS_STATS_ACTIVE|VIR_CONNECT_GET_ALL_DOMAINS_STATS_INACTIVE;

    virConnectPtr conn = NULL;

    virDomainPtr dom = NULL;

    virDomainStatsRecordPtr *retStats;

    conn = virConnectOpenReadOnly(NULL);
     
    if (conn == NULL) {

        fprintf(stderr, "Failed to connect to hypervisor\n");

        return 1;

    }


   ret = virConnectGetAllDomainStats(conn,stats,&retStats,flags);
   for(i=0;i<ret;i++){
       dom=retStats[i]->dom;
       const char *dom_name = virDomainGetName(dom);
       if(strcmp(name,dom_name)!=0){
           continue;
}
        params=retStats[i]->params;
	nparams=retStats[i]->nparams;
   }

    virDomainStatsRecordListFree(retStats);


    if (conn != NULL){

        virConnectClose(conn);

    }

    return 0;

}

