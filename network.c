#include<stdio.h>

#include<libvirt/libvirt.h>

 void getNetworkName(virTypedParameterPtr params,int nparams){
     const char * value;
     const char * name="net.0.name";
     int ret;
     ret=virTypedParamsGetString(params,nparams,name,&value);
     if(ret==1){
         printf("name: %s\n",value);
     }
return;
}

void getNetworkRxBytes(virTypedParameterPtr params,int nparams){
     unsigned long long value[1];
     const char * name="net.0.rx.bytes";
     int ret;
     ret=virTypedParamsGetULLong(params,nparams,name,value);
     if(ret==1){
         printf("RX-Bytes: %ld\n",value[0]);
     }
}

void getNetworkRxPkts(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.rx.pkts";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("RX-Pkts: %ld\n",value[0]);
    }
}

void getNetworkRxErrs(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.rx.errs";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("RX-Errs: %ld\n",value[0]);
    }
}

void getNetworkRxDrop(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.rx.drop";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("RX-Drop: %ld\n",value[0]);
    }
}

void getNetworkTxBytes(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.tx.bytes";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("TX-Bytes: %ld\n",value[0]);
    }
}

void getNetworkTxPkts(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.tx.pkts";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("TX-Pkts: %ld\n",value[0]);
    }
}

void getNetworkTxErrs(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.tx.errs";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("TX-Errs: %ld\n",value[0]);
    }
}

void getNetworkTxDrop(virTypedParameterPtr params,int nparams){
    unsigned long long value[1];
    const char * name="net.0.tx.drop";
    int ret;
    ret=virTypedParamsGetULLong(params,nparams,name,value);
    if(ret==1){
        printf("TX-Drop: %ld\n",value[0]);
    }
}





int getDomainInfo() {
    int ret,i;

    unsigned int stats=VIR_DOMAIN_STATS_INTERFACE;

    unsigned int flags=VIR_CONNECT_GET_ALL_DOMAINS_STATS_ACTIVE|VIR_CONNECT_GET_ALL_DOMAINS_STATS_INACTIVE;

    virConnectPtr conn = NULL;

    virDomainPtr dom = NULL;

    virDomainStatsRecordPtr *retStats;

    conn = virConnectOpenReadOnly(NULL);
     
    if (conn == NULL) {

        fprintf(stderr, "Failed to connect to hypervisor\n");

        return 1;

    }


   ret = virConnectGetAllDomainStats(conn,stats,&retStats,flags);
   for(i=0;i<ret;i++){
       dom=retStats[i]->dom;
       const char *dom_name = virDomainGetName(dom);
       printf("name:%s\n",dom_name);

        unsigned int * value;
        const char * name="net.count";
        virTypedParameterPtr params;
        int aaa,nparams;
        params=retStats[i]->params;
	    nparams=retStats[i]->nparams;
        aaa=virTypedParamsGetUInt(params,nparams,name,value);
	    if(aaa==1){
            printf("count:%d\n",value[0]);
	    }
	    getNetworkName(params,nparams);
      getNetworkRxBytes(params,nparams);
       getNetworkRxDrop(params,nparams);
       getNetworkRxErrs(params,nparams);
       getNetworkRxPkts(params,nparams);
       getNetworkTxBytes(params,nparams);
       getNetworkTxDrop(params,nparams);
       getNetworkTxErrs(params,nparams);
       getNetworkTxPkts(params,nparams);
   }

    virDomainStatsRecordListFree(retStats);


    if (conn != NULL){

        virConnectClose(conn);

    }

    return 0;

}

int main(int argc, char **argv)

{

    printf("-----Get network via libvirt C API -----\n");

    getDomainInfo();

    return 0;

}

